namespace testes;

[TestClass]
public class UnitTest1
{
    [TestMethod]
    public void TestContadorWithString()
    {
        FrequencyCounter<string> contador = new();

        contador.Register("oi");
        contador.Register("oi");
        contador.Register("tudo");
        contador.Register("bem");
        var expected = new List<KeyValuePair<string,int>>() {
            new KeyValuePair<string, int>("oi", 2),
            new KeyValuePair<string, int>("tudo", 1),
            new KeyValuePair<string, int>("bem", 1),
        };
        CollectionAssert.AreEqual(expected, contador.Top(3));
    }
    [TestMethod]
    public void testContadorWithInt() {

        FrequencyCounter<int> contador = new();

        contador.Register(1);
        contador.Register(1);
        contador.Register(2);
        contador.Register(3);

        var expected = new List<KeyValuePair<int, int>>() {
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(3, 1),
        };
        CollectionAssert.AreEqual(expected, contador.Top(3));
    }

    [TestMethod]
    public void TestwordCount() {
        WordCount wordCount = new();

        string path = @"C:\Users\phsou\Documents\raro_academy\exercicios\wordCount\text2.txt";

        wordCount.ReadFileAndCountWordFrequency(path);
        var expected = new List<KeyValuePair<string,int>>() {
            new KeyValuePair<string, int>("oi", 3),
            new KeyValuePair<string, int>("ola", 2),
            new KeyValuePair<string, int>("tudo", 1),
            new KeyValuePair<string, int>("bem", 1),
        };
       
        CollectionAssert.AreEqual(expected, wordCount.Top(4));
    }

    [TestMethod]
    public void TestLottery() {
        Lottery megaSena = new();

        int numberOfDraws = 3;

        megaSena.MaxValueRange = 1;

        megaSena.DrawNumbersAndCountFrequency(numberOfDraws);

        var expected = new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 3),
        };

        CollectionAssert.AreEqual(expected, megaSena.Top(1));

    }
    
}