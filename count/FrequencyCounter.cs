
namespace Shakspeare;

public class FrequencyCounter<Tkey>
{

    public Dictionary<Tkey, int> Dict = new();

    public void Register(Tkey key) {
        if(Dict.Keys.Contains(key)) {
            Dict[key]++;
        } else {
            Dict.Add(key, 1);
        }
    }

    public List<KeyValuePair<Tkey, int>> Top(int amount)
    {
        return (
            from entry in Dict
            orderby entry.Value descending
            select entry
        ).Take(amount).ToList();
    }

    // public void readAndCountWords()
    // {
    //     string path = @"C:\Users\phsou\Documents\raro_academy\exercicios\wordCount\Text.txt";
    //     try
    //     {
    //         using (StreamReader sr = File.OpenText(path))
    //         {
    //             while (!sr.EndOfStream)
    //             {
    //                 //string line = Regex.Replace(sr.ReadLine().Trim(), @"[^\w\d ]", "");
    //                 List<string> words = sr.ReadLine().Split(new char[] { ' ', '!', ',', '.', '-', ';', '?', ':' }).ToList();
    //                 for (int i = 0; i < words.Count; i++)
    //                 {
    //                     if (CountWords.Keys.Contains(words[i]))
    //                     {
    //                         CountWords[words[i]]++;
    //                     }
    //                     else if (words[i] != string.Empty)
    //                     {
    //                         CountWords.Add(words[i], 1);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     catch (IOException e)
    //     {
    //         Console.WriteLine(e.Message);
    //     }
    // }

    
}