namespace Shakspeare {
   public class Lottery : FrequencyCounter<int> {

        public int MaxValueRange { get; set; }

        public void DrawNumbersAndCountFrequency(int numberOfDraws) {
            Random rand = new Random();
            for(int i = 0; i < numberOfDraws; i++) {
                Register(rand.Next(0, MaxValueRange) + 1);
            }
        }
   }
}