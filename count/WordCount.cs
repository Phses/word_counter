namespace Shakspeare;

public class WordCount : FrequencyCounter<string>
{
    public void ReadFileAndCountWordFrequency(string path)
    {
         try
        {
            using (StreamReader sr = File.OpenText(path))
            {
                while (!sr.EndOfStream)
                {
                    //string line = Regex.Replace(sr.ReadLine().Trim(), @"[^\w\d ]", "");
                    List<string> words = sr.ReadLine().Split(new char[] { ' ', '!', ',', '.', '-', ';', '?', ':' }).ToList();
                    foreach(var word in words) {
                        if(word != string.Empty) {
                            Register(word);
                        }
                    }
                }
            }
        }
        catch (IOException e)
        {
            Console.WriteLine(e.Message);
        }
    }
}